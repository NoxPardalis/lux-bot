use dotenv::dotenv;
use serenity::{
    framework::standard::{
        macros::{command, group},
        CommandResult, StandardFramework,
    },
    model::{channel::Message, gateway::Ready},
    prelude::{Context, EventHandler},
    Client,
};

use serenity::prelude::*;

use std::sync::Arc;

use serenity::client::bridge::voice::ClientVoiceManager;

struct VoiceManager;

impl TypeMapKey for VoiceManager {
    type Value = Arc<Mutex<ClientVoiceManager>>;
}

group!({
    name: "general",
    options: {},
    commands: [ping, play, rank, join, leave],
});

#[command]
fn join(ctx: &mut Context, msg: &Message) -> CommandResult {
    let guild = match msg.guild(&ctx.cache) {
        Some(guild) => guild,
        None => {
            msg.reply(&ctx, "Group and DMs not supported")?;

            return Ok(());
        }
    };

    let guild_id = guild.read().id;

    let channel_id = guild
        .read()
        .voice_states
        .get(&msg.author.id)
        .and_then(|voice_states| voice_states.channel_id);

    let connect_to = match channel_id {
        Some(channel) => channel,
        None => {
            msg.reply(&ctx, "Not in a Voice Channel")?;

            return Ok(());
        }
    };

    let manager_lock = ctx.data.read().get::<VoiceManager>().cloned().unwrap();
    let mut manager = manager_lock.lock();

    match manager.join(guild_id, connect_to) {
        Some(_) => {
            msg.reply(&ctx, &format!("Joined {}", connect_to.mention()))?;
        }
        None => {
            msg.reply(&ctx, "Error joining channel.")?;
        }
    }

    Ok(())
}

#[command]
fn ping(ctx: &mut Context, msg: &Message) -> CommandResult {
    msg.reply(ctx, "Pong!")?;

    Ok(())
}

#[command]
fn leave(ctx: &mut Context, msg: &Message) -> CommandResult {
    let guild = match msg.guild(&ctx.cache) {
        Some(guild) => guild,
        None => {
            msg.reply(&ctx, "Group and DMs not supported")?;

            return Ok(());
        }
    };
    let guild_id = guild.read().id;
    let manager_lock = ctx.data.read().get::<VoiceManager>().cloned().unwrap();
    let mut manager = manager_lock.lock();
    match manager.get(guild_id) {
        Some(_) => {
            manager.remove(guild_id);
            msg.reply(&ctx, "Left voice channel")?;
        }
        None => {
            msg.reply(&ctx, "Not in a voice channel")?;
        }
    }
    Ok(())
}

#[command]
fn play(ctx: &mut Context, msg: &Message) -> CommandResult {
    msg.reply(ctx, "Playing Music!")?;

    Ok(())
}

#[command]
fn rank(ctx: &mut Context, msg: &Message) -> CommandResult {
    msg.reply(ctx, "Your rank is 0!")?;

    Ok(())
}

struct Handler;

impl EventHandler for Handler {
    fn ready(&self, _ctx: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
    }
}

fn main() {
    // Setup variables from optional `.env` file.
    dotenv().ok();
    // Get token from environment.
    let token = std::env::var("DISCORD_TOKEN")
        .expect("Expected the DISCORD_TOKEN environment variable to be set");
    // Create a client with the token and the behavior of `Handler`.
    let mut client = Client::new(&token, Handler).expect("Error creating the client");

    {
        let mut data = client.data.write();
        data.insert::<VoiceManager>(Arc::clone(&client.voice_manager));
    }

    client.with_framework(
        StandardFramework::new()
            .configure(|c| c.prefix("!"))
            .group(&GENERAL_GROUP),
    );
    // Attempt to start the bot client.
    client.start().expect("Error starting the client");
}
