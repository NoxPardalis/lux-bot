FROM clux/muslrust as build
RUN cargo install cargo-build-deps
WORKDIR /usr/build
COPY Cargo.toml Cargo.lock /usr/build/
RUN mkdir -p /usr/build/src && echo 'fn main() {}' >> /usr/build/src/main.rs
RUN cargo fetch
RUN cargo build-deps --release
COPY src /usr/build/src
RUN cargo build --release

FROM scratch
COPY --from=build /usr/build/target/x86_64-unknown-linux-musl/release/lux_bot /bin/
CMD ["lux_bot"]
